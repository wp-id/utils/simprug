simprug
=======

Watch new JSON files and execute commands with dynamic variables from the JSON file content when a new JSON file is created.

### Install

#### From binaries

Download your preferred binary from the [releases page](https://gitlab.com/wp-id/utils/simprug/-/releases).

#### From go get

```
go get gitlab.com/wp-id/utils/simprug
```

### Usage

Given `/tmp/simprug.yml`:

```
src: /home/akeda/json-files/
workDir: /home/akeda
commands:
    - 'echo {{name}} first item: {{items.0.name}}'
```

start `simprug` with:

```
simprug -c /tmp/simprug.yml
```

Create a new file `/home/akeda/json-files/test.json` (filename must be `.json`) with following content:

```json
{
  "name": "todo",
  "items": [
    {
      "name": "try simprug"
    }
  ]
}
```

```
2019/09/28 19:57:22 new file /home/akeda/json-files/test.json
2019/09/28 19:57:22 exec `echo todo first item: try simprug` in dir /home/akeda
2019/09/28 19:57:22 exec output `echo todo first item: try simprug`: todo first item: try simprug
```
