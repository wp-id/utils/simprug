package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"github.com/radovskyb/watcher"
	"github.com/tidwall/gjson"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	Src      string   `yaml:"src"`
	WorkDir  string   `yaml:"workDir"`
	Commands []string `yaml:"commands"`
}

var gjsonRe = regexp.MustCompile(`{{[\w\.]+}}`)

var configFile = flag.String("c", "./simprug.yml", "")

var usage = `Usage: simprug [options...]

Options:
  -c  Config file. Default to ./simprug.yml
`

func main() {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, usage)
	}
	flag.Parse()

	content, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalf("failed to read config file: %s", err)
	}

	var config Config
	if err = yaml.Unmarshal(content, &config); err != nil {
		log.Fatalf("failed to parse config: %s", err)
	}

	w := watcher.New()
	w.FilterOps(watcher.Create)
	w.AddFilterHook(watcher.RegexFilterHook(regexp.MustCompile("\\.json$"), false))

	if err := w.Add(config.Src); err != nil {
		log.Fatalf("failed to watch src %s: %s", config.Src, err)
	}

	go watch(w, config)
	if err := w.Start(time.Millisecond * 100); err != nil {
		log.Fatalf("failed to watch: %s", err)
	}
}

func watch(w *watcher.Watcher, config Config) {
	for {
		select {
		case event := <-w.Event:
			log.Printf("new file %s", event.Path)
			handleNewFile(event.Path, config)
		case err := <-w.Error:
			log.Fatalln(err)
		case <-w.Closed:
			return
		}
	}
}

func handleNewFile(path string, config Config) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(err)
		return
	}
	json := string(b)

	if !gjson.Valid(json) {
		log.Printf("invalid json in %s", path)
		return
	}

	for _, c := range config.Commands {
		cmd := replaceVars(c, json)
		fields := strings.Fields(cmd)
		if len(fields) == 0 {
			continue
		}

		runCmd(fields[0], config.WorkDir, fields[1:])
	}
}

func replaceVars(cmd, json string) string {
	vars := gjsonRe.FindAllString(cmd, -1)
	for _, v := range vars {
		r := gjson.Get(json, v[2:len(v)-2])
		cmd = strings.Replace(cmd, v, r.String(), -1)
	}

	return cmd
}

func runCmd(name, dir string, args []string) {
	c := exec.Command(name, args...)
	c.Dir = dir

	fullCmd := name + " " + strings.Join(args, " ")
	log.Printf("exec `%s` in dir %s", fullCmd, dir)
	if b, err := c.CombinedOutput(); err != nil {
		log.Printf("exec error `%s`: %s", name, err)
	} else {
		log.Printf("exec output `%s`: %s", fullCmd, string(b))
	}
}
